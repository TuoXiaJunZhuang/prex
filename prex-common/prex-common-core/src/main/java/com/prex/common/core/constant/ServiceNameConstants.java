package com.prex.common.core.constant;

/**
 * @Classname ServiceNameConstants
 * @Description 服务名称
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-09 10:52
 * @Version 1.0
 */
public class ServiceNameConstants {


    /**
     * 认证中心
     */
    public static final String AUTH_SERVICE = "prex-auth";

    /**
     * 系统UMPS模块
     */
    public static final String SYSTEM_UMPS_SERVICE = "prex-system-base-server";
}
