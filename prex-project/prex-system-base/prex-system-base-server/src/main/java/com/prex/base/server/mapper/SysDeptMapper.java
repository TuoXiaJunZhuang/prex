package com.prex.base.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prex.base.api.entity.SysDept;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
