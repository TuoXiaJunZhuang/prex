<div align="center">
<div style="height:256px; width:256px; text-align: center;">
<img src="https://gitee.com/li_haodong/picture_management/raw/master/pic/WechatIMG9.png" height="256" width="256">
</div>
       <a href="http://spring.io/projects/spring-boot">
            <img src="https://img.shields.io/badge/spring--boot-2.1.8.RELEASE-green.svg" alt="spring-boot">
       </a>
       <a href="http://spring.io/projects/spring-cloud">
            <img src="https://img.shields.io/badge/spring--cloud-Greenwich SR2-green.svg" alt="spring-cloud">
       </a>
       <a href="https://nacos.io/en-us/">
            <img src="https://img.shields.io/badge/spring--cloud--alibaba-2.1.0.RELEASE-brightgreen" alt="spring-cloud-alibaba">
       </a> <br>
       <a href="https://oauth.net/2/">
            <img src="https://img.shields.io/badge/OAuth-2.0-red" alt="oauth">
       </a>
       <a href="http://mp.baomidou.com">
            <img src="https://img.shields.io/badge/mybatis--plus-3.1.2-blue.svg" alt="mybatis-plus">
        </a>  
       <a href="https://spring.io/projects/spring-security">
            <img src="https://img.shields.io/badge/security-5.1.5-blue.svg" alt="security">
       </a>
       <a href="https://swagger.io/">
            <img src="https://img.shields.io/badge/swagger-2.8.0-9cf" alt="swagger">
       </a>
</div>


# prex
**Prex**基于Spring Boot 2.1.8  、Spring Cloud Greenwich.SR3、Spring Cloud Alibaba、Spring Security 、Spring cloud Oauth2 、Vue的前后端分离的的RBAC权限管理系统，项目支持数据权限管理，支持后端配置菜单动态路由,第三方社交登录, 努力做最简洁的后台管理系统

#### 我们努力做最简洁的权限管理系统。

## 目录
- 我们能为您做的

- 关于我们

- 项目详情
    - 架构图
    - 技术栈
    - 基本功能
    - 系统体验
    - 项目源码
    - 项目特点
    - 软件架构
    
- 使用详情
    - 安装教程
    - 使用说明
    
- 系统预览

### 我们能为您做的
- 使用租户做数据隔离，为您提供一个统一认证的权限管理平台(微服务版暂不开放，请使用单机版)。
- 将Prex集成到微服务项目中，对您现有业务代码零侵入，实现认证和授权功能。
- 关注进群可终身免费解答问题。
- 工作流(待开发，敬请期待)
### pre单机版
单机版地址: https://gitee.com/li_haodong/pre
### 关于我们
扫码进群，一对一解答
<table>
    <tr>
        <td align="center">扫码添加作者</td>
        <td align="center">我的公众号</td>
    </tr>
    <tr>
    <td align="center"><img src="https://gitee.com/li_haodong/picture_management/raw/master/pic/WechatIMG2.jpeg" width="150"/>
    </td>
    <td align="center"><img src="https://gitee.com/li_haodong/picture_management/raw/master/pic/qrcode_for_gh_99ee464aac4f_258.jpg" width="150"/>
    </td>
    </tr>
    <tr>
    <td align="center"><img src="https://images.gitee.com/uploads/images/2019/0818/220722_22be284e_5158154.jpeg" width="150"/></td>
    <td align="center"><img src="https://images.gitee.com/uploads/images/2019/0818/220750_56d8b008_5158154.jpeg" width="150"/></td>
    </tr>
</table>

### 项目详情
#### 架构图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0920/135700_2716a18b_1758995.jpeg "19121568599596_.pic_hd.jpg")
#### 技术栈
- 基于 Spring Boot 2.1.8 、Spring Cloud、Spring Cloud Alibaba、Spring Security、OAuth2的RBAC权限管理系统。
- 基于Nacos做注册中心和配置中心。
- 基于Sentinel做方法限流和阻塞处理。
- 基于 Vue 前端框架 和最新Ant Design界面。
- 基于 Mybatis Plus 简化开发、数据隔离等。
- 项目均使用 Lambda 、Stream Api 的风格编码。
- 使用 Spring Social 三方登录。
- 提供Spring Cloud Admin 做项目可视化监控。
- 基于Swagger提供统一Api管理。

#### 基本功能
- 用户管理：该功能主要完成系统用户配置，提供用户基础配置(用户名、手机号邮箱等)以及部门角色等。
- 角色管理：权限菜单分配，以部门基础设置角色的数据权限范围。
- 菜单管理：后端配置实现菜单动态路由，支持多级菜单，操作权限，按钮权限标识等。
- 部门管理：配置系统组织架构，树形表格展示，可随意调整上下级。
- 岗位管理：根据部门配置所属职位
- 租户管理：提供统一认证对权限管理平台，按照租户进行数据隔离(微服务版暂不开放)。
- 社交账号管理：可以对绑定Prex系统对社交账号进行查看和解绑。
- 字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：状态(正常/异常)，性别(男/女)等。
- 日志管理：可以删除和查看用户操作的日志。
- 异常日志：记录异常日志，方便开发人员定位错误。

#### 系统体验

**体验地址**  [https://prex.52lhd.com/](https://prex.52lhd.com/)

**账号密码** ```admin/admin```


#### 项目源码

|     |   后端源码  |   前端源码  |
|---  |--- | --- |
|  码云   |  https://gitee.com/kaiyuantuandui/prex   |  https://gitee.com/kaiyuantuandui/prex-ui   |

#### 项目特点
- 前后端分离的微服务架构
- 使用Nacos、Sentinel、SpringCloud等最新流行组件和UI
- 可直接集成到企业微服务项目中
- 使用Gateway进行高性能的网关路由
- 独立的UPMS系统
- 使用JWT进行Token管理
- 提供插拔式密码和客户端两种模式到授权方式
- 对日志操作、短信、邮件、Redis、资源服务、Swagger均提供插拔式使用
- 代码大量采用中文注释，极其简洁风格，上手快、易理解
- 采用RESTFul API 规范开发
- 统一异常拦截，友好的错误提示
- 基于注解 + Aop切面实现全方位日记记录系统
- 基于Mybatis拦截器 + 策略模式实现数据权限控制
- 提供解决前后分离第三方社交登录方案 
- Spring Social集成Security实现第三方社交登录   
- 基于Mybatis-Plus实现SaaS多租户功能(微服务版暂不开放)  

#### 软件架构
>prex

>>├── prex-ui -- 前端工程  

>>├── prex-auth -- 认证和授权    

>>├── prex-gateway --网关   

>>├── prex-project   
>>>├── prex-system-base  
>>>>├── prex-system-base-api    
>>>>├── prex-system-base-server  --umps服务  

>>├── prex-visual --图形化模块  
>>>├── prex-monitor  --监控监控（目前支持redis）   

>>├── prex-common --公共模块  
>>>├── prex-common-auth --  公共授权  
>>>├── prex-common-core --  异常、工具等核心配置   
>>>├── prex-common-data --数据权限拦截   
>>>├── prex-common-log --  日志相关   
>>>├── prex-common-message --短信邮件相关  
>>>├── prex-common-redis --redis配置   
>>>├── prex-common-social --支持社交三方登录  
>>>├── prex-common-swagger --添加swagger组件   

>>├── prex-docs  --文档  
>>>  ├── sql   --导入sql  

###使用详情
#### 安装教程

1. **前端**：
2. **nginx**  
3. **Nacos**    
**下载地址**：https://github.com/alibaba/nacos/releases   切记下载1.1.3+版本  
**启动命令**：在bin目录下执行：Linux/Unix/Mac：sh startup.sh -m standalone  
**登录页面**：localhost:8848/nacos
4. **Sentinel**(选用)  
**下载地址** ：https://github.com/alibaba/Sentinel/releases/download/1.6.0/sentinel-dashboard-1.6.0.jar  
**其他版本**：https://github.com/alibaba/Sentinel/releases  
**启动命令** ：java -Dserver.port=8888 -Dcsp.sentinel.dashboard.server=localhost:8888 -jar sentinel-dashboard-1.6.0.jar  
**登录页面**：localhost:8888  
5. **Zipkin**(选用)  
**下载地址**：https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/   
**启动命令**：java -jar zipkin-server-2.12.9-exec.jar  
**登录页面**：localhost:9411  

#### 使用说明
1. 按照上述下载Nacos（必须）并运行，如果不需要sentinel可以不用下载，并且要把代码中关于sentinel注解和依赖注释掉。
2. 连接到一台MySql，并修改prex-auth和prex-system-base-server服务的连接地址用户名密码等。
3. 创建一个数据库名为 pre_2，并执行项目中的sql文件。
4. 连接一台Redis，如果是本地直接启动不用修改配置。
5. 启动所有项目，如不需要SpringCloudAdmin可不用启动该项目。
6. Swagger 访问地址 ：localhost:8002/swagger-ui.html ,需要注意，在postman请求http://localhost:8001/oauth/token?username=admin&grant_type=password&scope=select&client_id=prex-app&client_secret=123456&password=admin
 获取token 然后把token值前加上Bearer(有空格) ，(格式：Bearer token ，)声明在swagger全局变量里。
### 系统预览
![输入图片说明](https://images.gitee.com/uploads/images/2019/0920/150005_4ad7b08d_1758995.png "WechatIMG3090.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0920/150023_ca9980aa_1758995.png "WechatIMG3082.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0920/150037_790247a0_1758995.png "WechatIMG3083.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0920/150046_28c43a9d_1758995.png "WechatIMG3084.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0921/085729_cfb199d9_1758995.png "WechatIMG29.png")